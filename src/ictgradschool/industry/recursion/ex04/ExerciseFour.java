package ictgradschool.industry.recursion.ex04;

/**
 * Created by anhyd on 27/03/2017.
 */
public class ExerciseFour {

    /**
     * Returns the sum of all positive integers between 1 and num (inclusive).
     */
    public int getSum(int num) {
        if (num == 1) {
            return 1;
        }
        return num + getSum(num - 1);
    }


    /**
     * Returns the smallest value in the given array, between the given first (inclusive) and second (exclusive) indices
     *
     * @param nums        the array
     * @param firstIndex  the inclusive lower index
     * @param secondIndex the exclusive upper index
     */
    public int getSmallest(int[] nums, int firstIndex, int secondIndex) {
        if (firstIndex + 1 == secondIndex) {
            return Math.min(nums[firstIndex], nums[secondIndex - 1]);
        }

        return Math.min(nums[firstIndex], getSmallest(nums, firstIndex + 1, secondIndex));
    }

    /**
     * Prints all ints from n down to 1.
     */
    public void printNums1(int n) {
        if (n == 1) {
            System.out.println(1);
            return;
        }

        System.out.println(n);
        printNums1(n - 1);
    }

    /**
     * Prints all ints from 1 up to n.
     */
    public void printNums2(int n) {
        if (n == 1) {
            System.out.println(1);
            return;
        }

        printNums2(n - 1);
        System.out.println(n);
    }

    /**
     * Returns the number of 'e' and 'E' characters in the given String.
     *
     * @param input the string to check
     */
    public int countEs(String input) {


        input = input.toUpperCase();
        if (input.length() == 0) {
            return 0;
        }

        if (input.charAt(input.length() - 1) == 'E') {
            return (1 + countEs(input.substring(0, input.length() - 1)));
        } else {
            return (countEs(input.substring(0, input.length() - 1)));
        }
    }

    /**
     * Returns the nth number in the fibonacci sequence.
     */
    public int fibonacci(int n) {
if (n == 1){
    return 1;
} else if (n==0){
    return 0;
}

        return fibonacci(n-1) + fibonacci(n-2);
    }

    /**
     * Returns true if the given input String is a palindrome, false otherwise.
     *
     * @param input the String to check
     */
    public boolean isPalindrome(String input) {
 if (input.length() <= 1){
     return true;
 }

        if (input.charAt(0)==input.charAt(input.length()-1)){
            return isPalindrome(input.substring(1, input.length()-1));
        }
return false;
    }

}
